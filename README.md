# Unpaired Image-to-Image Style Transfer

Python (Tensorflow) implementation of image-to-image style transfer model with CycleGAN. The model transfers styles between usual photos and Vincent van Gogh paintings in both directions. Created as a semestral project within MI-MVI.

Please, take a look at the report.

Dataset is included in the repository at folder `vangogh2photo`.

Jupyter notebook `style-transfer.ipynb` contains whole implementation.

All results from the report are available in folders `a_b_processing` and `b_a_processing`.
